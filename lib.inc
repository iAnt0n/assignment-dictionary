extern exit
extern string_length
extern print_string_out
extern print_string_err
extern print_error
extern print_newline
extern print_char
extern print_int
extern print_uint
extern string_equals
extern read_char
extern read_word
extern parse_uint
extern parse_int
extern string_copy
extern read_line

%define newline 0xA