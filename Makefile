.PHONY: all clean
AS=nasm
ASFLAGS=-felf64 -g
LD=ld

all: dictionary

lib.o: lib.asm
	$(AS) $(ASFLAGS) -o $@ $<

dict.o: dict.asm
	$(AS) $(ASFLAGS) -o $@ $<

main.o: main.asm words.inc
	$(AS) $(ASFLAGS) -o $@ $<

dictionary: dict.o lib.o main.o
	$(LD) -o $@ $^

clean:
	rm lib.o dict.o main.o dictionary

