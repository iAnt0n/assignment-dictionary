%ifndef COLON
%define COLON

%define tail 0

%macro colon_put 2
    %ifid %2
        %%some_label: dq tail
        db %1, 0
        %2: 
        %define tail %%some_label
    %else
        %error "Second argument of colon macro must be a valid label"
    %endif
%endmacro

%define colon_stringify(&str, label) colon_put str, label

%macro colon 2
    %ifstr %1
        colon_put %1, %2
    %else
        colon_stringify(%1, %2)
    %endif
%endmacro

%endif