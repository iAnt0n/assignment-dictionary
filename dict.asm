%include "lib.inc"

section .text

global find_word

find_word:
.loop:
    test rsi, rsi
    jz .not_found

    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .found
    mov rsi, [rsi]
    jmp .loop
.not_found:
    xor rax, rax
    ret
.found:
    add rsi, 8
    mov rax, rsi
    ret
