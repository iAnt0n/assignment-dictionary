%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .rodata
not_found_msg: db "There is no entry with such key", newline, 0
invalid_key_msg: db "Key cannot be empty or longer than 255 characters", newline, 0

section .text

global _start


%define buf_size 256
_start:
    sub rsp, buf_size
    mov rdi, rsp
    mov rsi, buf_size
    call read_line
    test rax, rax
    jz .invalid_key

    mov rdi, rsp
    mov rsi, tail
    call find_word
    test rax, rax
    jz .not_found

    push rax
    mov rdi, rax
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string_out
    call print_newline
    call exit
.not_found:
    mov rdi, not_found_msg
    call print_string_err
    mov rdi, 1
    call exit
.invalid_key:
    mov rdi, invalid_key_msg
    call print_string_err
    mov rdi, 1
    call exit


